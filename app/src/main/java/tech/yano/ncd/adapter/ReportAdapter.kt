package tech.yano.ncd.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.uzzal.core.inflate
import kotlinx.android.synthetic.main.item_report.view.*
import tech.yano.ncd.R
import tech.yano.ncd.db.entities.Report

class ReportAdapter(private val list:List<Report>):RecyclerView.Adapter<ReportAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ReportAdapter.ViewHolder, position: Int)=holder.bind(list[position])
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportAdapter.ViewHolder = ReportAdapter.ViewHolder(parent.inflate(R.layout.item_report))
    override fun getItemCount(): Int = list.size

    class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        fun bind(item:Report)=with(itemView){
            tvMonth.text = item.month
            lblTestType.text = item.type
            tvTestValue.text = item.total.toString()
        }
    }
}