package tech.yano.ncd.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.uzzal.core.inflate
import kotlinx.android.synthetic.main.item_patient.view.*
import tech.yano.ncd.R
import tech.yano.ncd.db.entities.Patient

class PatientAdapter(private val list:List<Patient>, private val listener:(Patient)->Unit):RecyclerView.Adapter<PatientAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int)=holder.bind(list[position], listener)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(parent.inflate(R.layout.item_patient))
    override fun getItemCount(): Int = list.size

    class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        fun bind(item:Patient, listener:(Patient)->Unit)= with(itemView){
            tvTestDate.text = item.name
            patientSex.text = item.sex
            patientAge.text = item.dob

            btnTest.setOnClickListener{
                listener(item)
            }
            setOnClickListener {
                listener(item)
            }
        }
    }
}