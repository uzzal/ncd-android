package tech.yano.ncd.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.google.gson.GsonBuilder
import com.uzzal.core.inflate
import com.uzzal.core.util.DateTools
import kotlinx.android.synthetic.main.item_test_result.view.*
import tech.yano.ncd.R
import tech.yano.ncd.db.entities.TestResult
import tech.yano.ncd.db.json.BloodPressure
import tech.yano.ncd.db.json.BloodSugar
import tech.yano.ncd.ui.Color

class ResultAdapter(private val list:List<TestResult>):RecyclerView.Adapter<ResultAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ResultAdapter.ViewHolder, position: Int)=holder.bind(list[position])
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultAdapter.ViewHolder = ResultAdapter.ViewHolder(parent.inflate(R.layout.item_test_result))
    override fun getItemCount(): Int = list.size

    class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        fun bind(item:TestResult)=with(itemView){
            if(item.type=="BLOOD_SUGAR"){
                groupBloodPressure.visibility=View.GONE
                groupBloodSugar.visibility=View.VISIBLE

                val result = GsonBuilder().create().fromJson<BloodSugar>(item.result, BloodSugar::class.java)

                if(result.fasting == "0"){
                    groupFasting.visibility = View.GONE
                }else{
                    groupFasting.visibility = View.VISIBLE
                    tvFasting.text = result.fasting
                    imgFasting.setImageResource(Color.fasting(result.fasting.toFloat()))
                }

                if(result.afterBreakfast=="0"){
                    groupAftarBreakfast.visibility = View.GONE
                }else{
                    groupAftarBreakfast.visibility = View.VISIBLE
                    tvAfterBreakfast.text = result.afterBreakfast
                    imgAfterBreakfast.setImageResource(Color.afterBreakfast(result.afterBreakfast.toFloat()))
                }

                if(result.random=="0"){
                    groupRandom.visibility = View.GONE
                }else{
                    groupRandom.visibility = View.VISIBLE
                    tvRandom.text = result.random
                    imgRandom.setImageResource(Color.random(result.random.toFloat()))
                }

                tvDateBloodSugar.text = DateTools.format(item.date)

            }else{
                groupBloodPressure.visibility=View.VISIBLE
                groupBloodSugar.visibility=View.GONE

                val result = GsonBuilder().create().fromJson<BloodPressure>(item.result, BloodPressure::class.java)
                tvDiastolic.text = result.distolic
                tvSystolic.text = result.systolic
                tvDateBloodPressure.text = DateTools.format(item.date)
                imgDiastolic.setImageResource(Color.diastolic(result.distolic.toFloat()))
                imgSystolic.setImageResource(Color.systolic(result.systolic.toFloat()))
            }
        }
    }
}