package tech.yano.ncd.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.uzzal.core.inflate
import kotlinx.android.synthetic.main.item_date.view.*
import tech.yano.ncd.R

class DateListAdapter(private val list:Map<String, String>, private val listener:()->Unit):RecyclerView.Adapter<DateListAdapter.ViewHolder>() {

    private val keys = list.keys.toList()

    override fun onBindViewHolder(holder: ViewHolder, position: Int){
        holder.bind(keys[position],list[keys[position]]!!, listener)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(parent.inflate(R.layout.item_date))
    override fun getItemCount(): Int = list.size

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind(date: String, type:String, listener:()->Unit)= with(itemView){
            tvTestDate.text = date

            if(type.contains("BLOOD_SUGAR")){
                blood_sugar.visibility=View.VISIBLE
            }else{
                blood_sugar.visibility=View.INVISIBLE
            }

            if(type.contains("BLOOD_PRESSURE")){
                blood_pressure.visibility=View.VISIBLE
            }else{
                blood_pressure.visibility=View.INVISIBLE
            }

            setOnClickListener {
                listener()
            }
        }
    }
}