package tech.yano.ncd.sync

import android.content.Context
import android.widget.Toast
import com.uzzal.core.RestClient
import com.uzzal.core.Token
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tech.yano.ncd.db.Db
import tech.yano.ncd.db.entities.Report
import tech.yano.ncd.db.entities.TestResult
import tech.yano.ncd.http.TestResultRequest
import tech.yano.ncd.http.TestResultResponse
import kotlin.concurrent.thread

class SyncUtil {
    fun syncTestResult(context:Context, id:Long, testResult:TestResult){
        val token = Token.init(context!!).token!!
        val request = TestResultRequest(
                token,
                testResult.patientId,
                testResult.ownerId,
                testResult.type,
                testResult.date,
                testResult.result
        )
        val caller = RestClient.httpPOST.submitTestResult(request)

        caller.enqueue(object:Callback<TestResultResponse>{
            override fun onFailure(call: Call<TestResultResponse>?, t: Throwable?) {
                println("failed network") //todo remove it
            }

            override fun onResponse(call: Call<TestResultResponse>?, response: Response<TestResultResponse>?) {
                if(response!!.isSuccessful){
                    thread {
                        val dao = Db.init(context).testResultDao()
                        val result = dao.find(id)
                        result.remoteId = response.body()?.remoteId?.toIntOrNull()
                        result.sync="done"

                        dao.update(result)
                        println("result data updated with remote id:"+result.remoteId) //todo remove it
                    }
                }
            }
        })
    }

    fun agentReport(context: Context, all:Boolean=false){
        val token = Token.init(context).token
        val caller = RestClient.httpGET.agentReport(all, token!!)
        caller.enqueue(object:Callback<List<Report>>{
            override fun onFailure(call: Call<List<Report>>?, t: Throwable?) {
                Toast.makeText(context, "Report refreshing failed due to network error!", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<List<Report>>?, response: Response<List<Report>>?) {
                if(response!!.isSuccessful){
                    thread {
                        val dao = Db.init(context).reportDao()
                        dao.insert(response.body()!!)
                    }
                }else{
                    Token.init(context).clear()
                }
            }
        })
    }
}