package tech.yano.ncd.http

import com.google.gson.annotations.SerializedName

class TestResultResponse(
        val status:String,
        @SerializedName("remote_id")
        val remoteId:String
)