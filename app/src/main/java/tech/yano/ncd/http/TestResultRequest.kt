package tech.yano.ncd.http

import com.google.gson.annotations.SerializedName

data class TestResultRequest(
        @SerializedName("_token")
        var token:String,
        @SerializedName("patient_id")
        var patientId:Int,
        @SerializedName("owner_id")
        var ownerId:Int,
        var type:String,
        var date:String,
        var result:String
)