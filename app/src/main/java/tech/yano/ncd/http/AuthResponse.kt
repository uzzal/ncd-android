package tech.yano.ncd.http

import com.google.gson.annotations.SerializedName
import tech.yano.ncd.db.entities.User

data class AuthResponse(
        var status:String,
        var token:String,
        @SerializedName("user_id") var userId:Int,
        var user:User
)