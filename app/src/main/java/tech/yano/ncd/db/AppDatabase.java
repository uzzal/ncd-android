package tech.yano.ncd.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import tech.yano.ncd.db.entities.Report;
import tech.yano.ncd.db.entities.TestResult;

@Database(entities = {TestResult.class, Report.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract TestResultDao testResultDao();
    public abstract ReportDao reportDao();
}
