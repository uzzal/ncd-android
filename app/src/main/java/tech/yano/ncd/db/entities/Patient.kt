package tech.yano.ncd.db.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Patient(
        @SerializedName("patient_id") val patientId:Int,
        val name:String,
        val address:String,
        val phone:String,
        val mobile:String,
        val sex:String,
        val dob:String,
        @SerializedName("test_dates")
        val testDates:ArrayList<String>
):Serializable