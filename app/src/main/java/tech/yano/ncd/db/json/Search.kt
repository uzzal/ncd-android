package tech.yano.ncd.db.json

data class Search(
        var patient_id:String,
        var email:String,
        var mobile:String
) {
    constructor():this("","","")
}