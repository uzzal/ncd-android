package tech.yano.ncd.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy

import tech.yano.ncd.db.entities.Patient

@Dao
interface PatientDao
