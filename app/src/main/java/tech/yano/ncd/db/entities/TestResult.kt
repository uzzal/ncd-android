package tech.yano.ncd.db.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "test_results", indices = [(Index(value = ["remote_id"], unique = true))])
data class TestResult(
    @PrimaryKey(autoGenerate = true)
    var id:Int?=null,
    @ColumnInfo(name="remote_id")
    var remoteId:Int?=null,
    @ColumnInfo(name = "patient_id")
    var patientId:Int,
    @ColumnInfo(name = "owner_id")
    var ownerId:Int,
    var type:String,
    var date:String,
    var result:String,
    var sync:String
){
    constructor():this(null, null, 0, 0, "", "", "", "")
}