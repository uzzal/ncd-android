package tech.yano.ncd.db.entities

import com.google.gson.annotations.SerializedName

data class User (
        @SerializedName("user_id") val userId:Int,
        val name:String,
        val email:String,
        @SerializedName("is_active") val isActive:Int,
        @SerializedName("created_at") val createdAt:String
)