package tech.yano.ncd.db

import android.arch.persistence.room.Room
import android.content.Context


object Db{
    private lateinit var me: AppDatabase

    @JvmStatic
    fun init(context: Context): AppDatabase {
        if (!::me.isInitialized) {
            println("init db")
            me = Room.databaseBuilder(context, AppDatabase::class.java, "ncd-db").build()
        }
        return me
    }
}