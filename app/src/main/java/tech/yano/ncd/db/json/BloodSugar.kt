package tech.yano.ncd.db.json

data class BloodSugar(
    var fasting:String,
    var afterBreakfast:String,
    var random:String
){
    constructor():this("","","")

    override fun toString(): String {
        return "BLOOD_SUGAR"
    }
}