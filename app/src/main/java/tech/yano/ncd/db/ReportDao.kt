package tech.yano.ncd.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import tech.yano.ncd.db.entities.Report

@Dao
interface ReportDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(reports: List<Report>)

    @Query("select * from reports")
    fun getAll():List<Report>
}