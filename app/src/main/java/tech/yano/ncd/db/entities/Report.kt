package tech.yano.ncd.db.entities

import android.arch.persistence.room.Entity

@Entity(tableName = "reports", primaryKeys = ["month", "type"])
data class Report(
        var month:String,
        var type:String,
        var total:Int
)