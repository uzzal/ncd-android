package tech.yano.ncd.db

import android.arch.persistence.room.*
import tech.yano.ncd.db.entities.Report
import tech.yano.ncd.db.entities.TestResult

@Dao
interface TestResultDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(testResult:TestResult):Long

    @Query("select * from test_results where patient_id=:patientId order by date desc, id desc")
    fun getAll(patientId:Int):List<TestResult>

    @Query("select * from test_results where id=:id")
    fun find(id:Long):TestResult

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun report(report:Report)

    @Update
    fun update(testResult: TestResult)
}