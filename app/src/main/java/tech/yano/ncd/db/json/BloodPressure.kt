package tech.yano.ncd.db.json

data class BloodPressure(
    var systolic:String,
    var distolic:String
){
    constructor():this("","")

    override fun toString(): String {
        return "BLOOD_PRESSURE"
    }
}