package tech.yano.ncd.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_datelist.*
import kotlinx.android.synthetic.main.content_datelist.*
import tech.yano.ncd.R
import tech.yano.ncd.adapter.DateListAdapter
import tech.yano.ncd.db.entities.Patient

class DatelistActivity : AppCompatActivity() {

    lateinit var patient: Patient
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_datelist)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        patient = intent.getSerializableExtra("patient") as Patient

        if(patient.testDates==null){
            noData.visibility= View.VISIBLE
            rvDateList.visibility = View.GONE
            noData.setOnClickListener {
                val intent = Intent(applicationContext, TestActivity::class.java)
                intent.putExtra("patient", patient)
                startActivity(intent)
            }
        }else {
            noData.visibility= View.GONE
            rvDateList.visibility = View.VISIBLE

            val map = plotMap(patient.testDates).toSortedMap()
            rvDateList.layoutManager = LinearLayoutManager(applicationContext)
            rvDateList.adapter = DateListAdapter(map) {
                val intent = Intent(applicationContext, TestActivity::class.java)
                intent.putExtra("patient", patient)
                startActivity(intent)
            }
        }
    }

    private fun plotMap(list:ArrayList<String>):Map<String, String>{
        val map = mutableMapOf<String, String>()
        list.forEach {
            val key:String = it.substringBefore('_')
            val value:String = it.substringAfter('_')
            if(map.contains(key)){
                if(!map[key].equals(value)){
                    map[key] = map[key]+", "+value
                }
            }else{
                map[key] = value
            }
        }
        return map
    }

}
