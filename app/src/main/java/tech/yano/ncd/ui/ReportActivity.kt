package tech.yano.ncd.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_report.*
import kotlinx.android.synthetic.main.content_report.*
import tech.yano.ncd.R
import tech.yano.ncd.adapter.ReportAdapter
import tech.yano.ncd.db.Db
import tech.yano.ncd.sync.SyncUtil
import kotlin.concurrent.thread


class ReportActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        /*val events = ArrayList()

        val calendar = Calendar.getInstance()
        //events.add(EventDay(calendar, R.drawable.sample_icon))

        val calendarView = findViewById<View>(R.id.calendarView) as CalendarView
        calendarView.setEvents(events)*/

        thread {
            val data = ReportAdapter(Db.init(applicationContext).reportDao().getAll())
            ReportActivity@this.runOnUiThread {
                rvReport.adapter = data
            }
        }
        rvReport.layoutManager = LinearLayoutManager(applicationContext)

        srlReport.setOnRefreshListener {
            srlReport.isRefreshing=false
            SyncUtil().agentReport(applicationContext, true)
            Toast.makeText(applicationContext, "Report refreshed!", Toast.LENGTH_LONG).show()
        }

    }

}
