package tech.yano.ncd.ui.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.View
import com.google.gson.GsonBuilder
import com.uzzal.core.Token
import com.uzzal.core.util.DateTools
import com.uzzal.core.util.JsonTools
import kotlinx.android.synthetic.main.dialog_blood_sugar.view.*
import tech.yano.ncd.R
import tech.yano.ncd.db.Db
import tech.yano.ncd.db.entities.Patient
import tech.yano.ncd.db.entities.TestResult
import tech.yano.ncd.db.json.BloodSugar
import tech.yano.ncd.sync.SyncUtil
import tech.yano.ncd.ui.TestActivity
import kotlin.concurrent.thread

class BloodSugarDialog:DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)

        val view:View = activity!!.layoutInflater.inflate(R.layout.dialog_blood_sugar, null)
        val patient = arguments?.get("patient") as Patient
        return AlertDialog.Builder(activity!!)
                .setTitle("Blood Sugar Test")
                .setView(view)
                .setPositiveButton("Save", {_, _->

                    val result = BloodSugar()

                    result.fasting = JsonTools.defaultValue(view.etFasting.text.toString(), "0")
                    result.afterBreakfast = JsonTools.defaultValue(view.etAfterBreakfast.text.toString(),"0")
                    result.random = JsonTools.defaultValue(view.etRandom.text.toString(),"0")

                    val testResult = TestResult()
                    testResult.patientId = patient.patientId
                    testResult.ownerId = Token.init(context!!).userId
                    testResult.date = DateTools.today()
                    testResult.type = result.toString()
                    testResult.result = GsonBuilder().create().toJson(result)

                    val ctx: Context = context!!
                    thread {
                        val id = Db.init(ctx).testResultDao().insert(testResult)
                        SyncUtil().syncTestResult(ctx, id, testResult)
                    }
                    (activity as TestActivity).dataRefresh(patient)
                })
                .setNegativeButton("Cancel", { dialog, _ ->
                    dialog.dismiss()
                })
                .create()

    }
}