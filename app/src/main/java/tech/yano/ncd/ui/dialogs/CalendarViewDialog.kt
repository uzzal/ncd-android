package tech.yano.ncd.ui.dialogs

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.View
import com.applandeo.materialcalendarview.EventDay
import kotlinx.android.synthetic.main.dialog_calendar.view.*
import tech.yano.ncd.R
import tech.yano.ncd.db.entities.Patient
import java.text.SimpleDateFormat
import java.util.*



class CalendarViewDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        val view: View = activity!!.layoutInflater.inflate(R.layout.dialog_calendar, null)

        val calendar = Calendar.getInstance()
        view.calendarView.setDate(calendar)

        val events = mutableListOf<EventDay>()
        val formatter = SimpleDateFormat("yyyy-MM-dd")

        val patient = arguments?.get("patient") as Patient

        patient.testDates?.forEach {
            events.add(event(it, formatter))
        }

        view.calendarView.setEvents(events)

        return AlertDialog.Builder(activity!!)
                .setView(view)
                .setNegativeButton("Close", { dialog, _ -> dialog.dismiss() })
                .create()
    }

    private fun event(date:String, formatter:SimpleDateFormat):EventDay{
        val calendar = Calendar.getInstance()
        calendar.time = formatter.parse(date)
        //return EventDay(calendar, R.drawable.both)
        return EventDay(calendar, R.drawable.ic_fiber_manual_record_black_24dp)
    }
}