package tech.yano.ncd.ui

object Color {
    private const val RED:Int = android.R.color.holo_red_dark
    private const val GREEN:Int = android.R.color.holo_green_dark
    private const val YELLOW:Int = android.R.color.holo_orange_dark

    fun fasting(value:Float):Int{
        return if(value<=5.5){
            GREEN
        }else if(value > 5.5 && value <= 6.9){
            YELLOW
        }else{
            RED
        }
    }

    fun afterBreakfast(value:Float):Int{
        return if(value<=7.8){
            GREEN
        }else if(value > 7.8 && value <= 11){
            YELLOW
        }else{
            RED
        }
    }

    fun random(value:Float):Int{
        return if(value<=11){
            GREEN
        }else{
            RED
        }
    }

    fun systolic(value:Float):Int{
        return if(value in 100.0..120.0){
            GREEN
        }else if(value > 120 && value <= 139){
            YELLOW
        }else{
            RED
        }
    }

    fun diastolic(value:Float):Int{
        return if(value in 60.0..80.0){
            GREEN
        }else if(value > 80 && value <= 90){
            YELLOW
        }else{
            RED
        }
    }
}