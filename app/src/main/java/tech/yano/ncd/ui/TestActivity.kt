package tech.yano.ncd.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_test.*
import kotlinx.android.synthetic.main.content_test.*
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.runBlocking
import tech.yano.ncd.R
import tech.yano.ncd.adapter.ResultAdapter
import tech.yano.ncd.db.Db
import tech.yano.ncd.db.entities.Patient
import tech.yano.ncd.db.entities.TestResult
import tech.yano.ncd.ui.dialogs.BloodPressureDialog
import tech.yano.ncd.ui.dialogs.BloodSugarDialog
import tech.yano.ncd.ui.dialogs.CalendarViewDialog

class TestActivity : AppCompatActivity() {

    lateinit var patient: Patient
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        patient = intent.getSerializableExtra("patient") as Patient

        tvTestDate.text = patient.name
        patientSex.text = patient.sex

        rvResultList.layoutManager = LinearLayoutManager(applicationContext)

        btnBloodSugar.setOnClickListener {
            val dialog = BloodSugarDialog()
            val bundle = Bundle()
            bundle.putSerializable("patient", patient)
            dialog.arguments = bundle
            dialog.show(supportFragmentManager, "blood_sugar_dialog")
        }

        btnBloodPressure.setOnClickListener {
            val dialog = BloodPressureDialog()
            val bundle = Bundle()
            bundle.putSerializable("patient", patient)
            dialog.arguments = bundle
            dialog.show(supportFragmentManager, "blood_pressure_dialog")
        }

        btnCalendar.setOnClickListener{
            val dialog = CalendarViewDialog()
            val bundle = Bundle()
            bundle.putSerializable("patient", patient)
            dialog.arguments = bundle
            dialog.show(supportFragmentManager, "calendar_dialog")
        }
    }

    override fun onResume() {
        super.onResume()
        dataRefresh(patient)
    }

    fun dataRefresh(patient: Patient){
        runBlocking {
            val list = async {
                getAllTestResults(patient.patientId)
            }

            rvResultList.swapAdapter(ResultAdapter(list.await()), true)
        }
    }

    private fun getAllTestResults(patientId:Int):List<TestResult>{
        return Db.init(applicationContext).testResultDao().getAll(patientId)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        super.onOptionsItemSelected(item)
        when(item?.itemId){
            android.R.id.home->{
                val intent = Intent(applicationContext, DatelistActivity::class.java)
                intent.putExtra("patient", patient)
                startActivity(intent)
                finish()
                return true
            }
        }

        return true
    }

}
