package tech.yano.ncd

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.facebook.stetho.Stetho
import com.google.android.gms.samples.vision.barcodereader.BarcodeCaptureActivity
import com.google.gson.GsonBuilder
import com.uzzal.core.RestClient
import com.uzzal.core.Token
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tech.yano.ncd.adapter.PatientAdapter
import tech.yano.ncd.db.entities.Patient
import tech.yano.ncd.db.json.Search
import tech.yano.ncd.sync.SyncUtil
import tech.yano.ncd.ui.DatelistActivity
import tech.yano.ncd.ui.LoginActivity
import tech.yano.ncd.ui.ReportActivity


class MainActivity : AppCompatActivity() {

    private val RC_BARCODE_CAPTURE = 9001
    private val TAG = "BarcodeMain"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        Stetho.initializeWithDefaults(applicationContext) //todo remove in production

        if(Token.init(applicationContext).token==null){
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }

        rPatientList.layoutManager = LinearLayoutManager(applicationContext)

        btnSearch.setOnClickListener {
            search(etSearch.text.toString())
        }

        val intent = intent
        if(intent.hasExtra("BARCODE")){
            val qr = GsonBuilder().create().fromJson<Search>(intent.getStringExtra("BARCODE"), Search::class.java)
            etSearch.setText(qr.mobile)
            search(qr.mobile)
        }

        fab.setOnClickListener { _ ->
            val intent = Intent(this, BarcodeCaptureActivity::class.java)
            startActivityForResult(intent, RC_BARCODE_CAPTURE)
        }
    }

    override fun onResume() {
        super.onResume()
        SyncUtil().agentReport(applicationContext, true)
    }

    private fun search(query:String) {
        progressBar.visibility = View.VISIBLE
        rPatientListWrap.visibility = View.GONE
        val caller = RestClient.httpGET.search(query, Token.init(applicationContext).token!!)
        caller.enqueue(object : Callback<List<Patient>> {
            override fun onResponse(call: Call<List<Patient>>?, response: Response<List<Patient>>?) {
                if (response!!.isSuccessful) {
                    rPatientList.swapAdapter(PatientAdapter(response.body()!!, {
                        //val intent = Intent(applicationContext, TestActivity::class.java)
                        val intent = Intent(applicationContext, DatelistActivity::class.java)
                        intent.putExtra("patient", it)
                        startActivity(intent)
                    }), true)
                }else{
                    Token.init(applicationContext).clear()
                    recreate()
                }
                progressBar.visibility = View.GONE
                rPatientListWrap.visibility = View.VISIBLE
            }

            override fun onFailure(call: Call<List<Patient>>?, t: Throwable?) {
                progressBar.visibility = View.GONE
                rPatientListWrap.visibility = View.VISIBLE
                AlertDialog.Builder(this@MainActivity)
                        .setTitle("Network Error!")
                        .setMessage("Sorry! Something went wrong while connecting to the Server, please try again in a moment!")
                        .setNegativeButton("Close", { d, _ ->
                            d.cancel()
                        })
                        .create()
                        .show()
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_logout -> {
                Token.init(applicationContext).clear()
                recreate()
                true
            }
            R.id.action_report->{
                startActivity(Intent(this, ReportActivity::class.java))
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }
}
