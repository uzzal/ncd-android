package com.uzzal.core

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import tech.yano.ncd.AppConfig

object RestClient{
    private val client:Retrofit by lazy{
        Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(AppConfig.BASE_URL)
                .build()
    }

    @JvmStatic
    val httpPOST:PostService by lazy{
        client.create(PostService::class.java)
    }

    @JvmStatic
    val httpGET:GetService by lazy{
        client.create(GetService::class.java)
    }
}