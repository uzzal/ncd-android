package com.uzzal.core

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import tech.yano.ncd.db.entities.Patient
import tech.yano.ncd.db.entities.Report

interface GetService {

    @GET("api/patient")
    fun search(@Query("q") query:String, @Query("_token") token:String): Call<List<Patient>>

    @GET("api/agent-report")
    fun agentReport(@Query("all") all:Boolean, @Query("_token") token:String): Call<List<Report>>
}