package com.uzzal.core.util

import android.os.Build
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

object DateTools {
    @JvmStatic
    fun today(pattern:String="yyyy-MM-dd HH:mm:ss"):String{
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ofPattern(pattern)
            current.format(formatter)
        } else {
            val formatter = SimpleDateFormat(pattern)
            formatter.format(Date())
        }
    }

    fun format(timestamp:String, outPattern:String="hh:mm a, MMM dd, yy", inPattern:String="yyyy-MM-dd HH:mm:ss"):String{
        return try {
            val dt = SimpleDateFormat(inPattern).parse(timestamp)
            SimpleDateFormat(outPattern).format(dt)
        } catch (e: ParseException) {
            timestamp
        }
    }
}