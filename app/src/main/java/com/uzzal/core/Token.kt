package com.uzzal.core

import android.content.Context
import android.content.SharedPreferences
import java.util.concurrent.atomic.AtomicBoolean

class Token private constructor(context: Context){
    private val pref:SharedPreferences = context.getSharedPreferences("_token", Context.MODE_PRIVATE)

    var token: String? = pref.getString("_token", null)
    var name:String? = pref.getString("name", null)
    var email:String? = pref.getString("email", null)
    var userId:Int = pref.getInt("user_id", 0)

    fun set(token:String, name:String, email:String, userId:Int){
        pref.edit()
            .putString("_token", token)
            .putString("name", name)
            .putString("email", email)
            .putInt("user_id", userId)
            .apply()

        this.token = token
        this.name = name
        this.email = email
        this.userId = userId
    }

    fun clear(){
        pref.edit().remove("_token").apply()
        token=null
    }

    companion object {
        private val created:AtomicBoolean=AtomicBoolean()
        private lateinit var me:Token

        @JvmStatic
        fun init(context: Context):Token{
            if(!created.getAndSet(true)){
                me = Token(context)
            }
            return me
        }
    }

}