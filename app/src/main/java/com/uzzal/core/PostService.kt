package com.uzzal.core

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import tech.yano.ncd.http.AuthRequest
import tech.yano.ncd.http.AuthResponse
import tech.yano.ncd.http.TestResultRequest
import tech.yano.ncd.http.TestResultResponse

interface PostService {
    @POST("api/auth")
    fun login(@Body auth:AuthRequest):Call<AuthResponse>

    @POST("api/test-result")
    fun submitTestResult(@Body result:TestResultRequest):Call<TestResultResponse>
}