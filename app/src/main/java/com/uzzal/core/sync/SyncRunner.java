package com.uzzal.core.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;

public class SyncRunner {
    // Constants
    // The authority for the sync adapter's content provider
    public static final String AUTHORITY = "tech.yano.ncd.provider";
    // An account type, in the form of a domain name
    public static final String ACCOUNT_TYPE = "ConnectedNCD";
    // The account name
    public static final String ACCOUNT = "ConnectedNCD";

    // Sync interval constants
    public static final long SECONDS_PER_MINUTE = 60L;
    public static final long SYNC_INTERVAL_IN_MINUTES = 1L;
    public static final long SYNC_INTERVAL = SYNC_INTERVAL_IN_MINUTES * SECONDS_PER_MINUTE;

    /**
     * Create a new dummy account for the sync adapter
     *
     * @param context The application context
     */
    public static Account CreateSyncAccount(Context context) {
        // Create the account type and default account
        Account newAccount = new Account(ACCOUNT, ACCOUNT_TYPE);
        // Get an instance of the Android account manager
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        if (accountManager.addAccountExplicitly(newAccount, null, null)) {
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call context.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
            ContentResolver.setSyncAutomatically(newAccount, AUTHORITY, true);
            System.out.println("sync account registered");
        } else {
            System.out.println("something went wrong in SyncRunner");
        }

        return newAccount;
    }

    public static void syncPeriodically(Context context){
        ContentResolver.addPeriodicSync(
                CreateSyncAccount(context),
                AUTHORITY,
                Bundle.EMPTY,
                SYNC_INTERVAL);
    }

    public static void syncImmediately(Context context){
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        /*
         * Request the sync for the default account, authority, and
         * manual sync settings
         */
        ContentResolver.requestSync(CreateSyncAccount(context), AUTHORITY, settingsBundle);
    }
}
